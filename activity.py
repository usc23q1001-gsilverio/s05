from abc import ABC, abstractclassmethod

class Animal(ABC):
    @abstractclassmethod
    def eat(self, food):
        pass

    def make_sound(self):
        pass

    def call(self, name):
        pass


class Cat(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age

    def eat(self, food):
        self.food = food.lower()
        print(f"The feline friend has eaten {self.food}.")

    def make_sound(self):
        print("Meow meow prr mrrow grrr MROW!")

    def call(self):
        print(f"Come hither, {self._name}!")

    def set_name(self, name):
        self._name = name

    def set_breed(self, breed):
        self._breed = breed

    def set_age(self, age):
        self._age = age

    def get_name(self):
        print(f"The cat's name is {self._name}.")

    def get_breed(self):
        print(f"The cat's breed is {self._breed}.")

    def get_age(self):
        print(f"The cat is {self._age} years old.")

class Dog(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age

    def eat(self, food):
        self.food = food.lower()
        print(f"The canine friend has eaten {self.food}.")

    def make_sound(self):
        print("ARF ARF ARF ARF ARF ARF (pant pant pant) ARF ARF ARF!")

    def call(self):
        print(f"Oi, {self._name}, get over here!")

    def set_name(self, name):
        self._name = name

    def set_breed(self, breed):
        self._breed = breed

    def set_age(self, age):
        self._age = age

    def get_name(self):
        print(f"The dog's name is {self._name}.")

    def get_breed(self):
        print(f"The dog's breed is {self._breed}.")

    def get_age(self):
        print(f"The dog is {self._age} years old.")

    
cat1 = Cat("Rotham", "Persian", 4)
cat1.eat("Cat Food")
cat1.make_sound()
cat1.call()

dog1 = Dog("Maisy", "Frenchie", 6)
dog1.eat("A Really Really Really Big Steak")
dog1.make_sound()
dog1.call()