# [SECTION] Python Class Review

# class SampleClass():
#     def __init__(self, year):
#         self.year = year
    
#     def show_year(self):
#         print(f'The year is: {self.year}')

# myObj = SampleClass(2022)

# print(myObj.year)
# myObj.show_year()

# [SECTION] Fundamentals of OOP

# -----------------------------------------------------------------

# [SECTION] Encapsulation

class Person():
    def __init__(self):
        self._name = 'Sample Name'
        self._age = 0

    def set_name(self, name):
        self._name = name

    def get_name(self):
        print(f'Name of Person: {self._name}')

    def set_age(self, age):
        self._age = age

    def get_age(self):
        print(f'Age of Person: {self._age}')

# p1 = Person()

# # p1.get_name()
# p1.set_name('Ja Morant')
# p1.get_name()

# # p1.get_age()
# p1.set_age(40)
# p1.get_age()

# -----------------------------------------------------------------

# [SECTION] Inheritance

class Employee(Person):
    def __init__(self, employeeId):
        # super() can be used invoke the immediate parent class constructor
        super().__init__()

        # unique attribute to the Employee class
        self._employeeId = employeeId

    # Methods of the Employee class
    def get_employeeId(self):
        print(f'The Employee\'s ID is {self._employeeId}')

    def set_EmployeeId(self, employeeId):
        self._employeeId = employeeId

    def get_details(self):
        print(f"{self._employeeId} belongs to {self._name}")

# emp1 = Employee("Emp-001")
# emp1.get_details()
# emp1.get_name()
# emp1.get_age()

# emp1.set_name("DNP Morant")
# emp1.set_age(12)
# emp1.get_details()

class Student(Person):
    def __init__(self, studentNum, course, yearLevel):
        super().__init__()

        self._studentNum = studentNum
        self._course = course
        self._yearLevel = yearLevel

    def get_studentNum(self):
        print(f'The Student\'s ID number is {self._studentNum}')

    def get_course(self):
        print(f'The Student\'s course is {self._course}')

    def get_yearLevel(self):
        print(f'The Student\'s year level is {self._yearLevel}')

    def set_studentNum(self, studentNum):
        self._studentNum = studentNum

    def set_course(self, course):
        self._course = course

    def set_yearLevel(self, yearLevel):
        self._yearLevel = yearLevel

    def get_details(self):
        print(f"{self._name} is currently in year {self._yearLevel} taking up {self._course}.")

# stud1 = Student("Sample", "Sample", "Sample")

# stud1.set_name("Student McStudentson")
# stud1.set_age(20)
# stud1.set_studentNum("Stud-001")
# stud1.set_course("Bachelors in Mont Blanc Studies")
# stud1.set_yearLevel(3)

# stud1.get_name()
# stud1.get_studentNum()
# stud1.get_course()
# stud1.get_yearLevel()
# stud1.get_details()

# -----------------------------------------------------------------

# [SECITON] Polymorphism

class Admin():
    def is_admin(self):
        print(True)

    def user_type(self):
        print('Admin User')

class Customer():
    def is_admin(self):
        print(False)

    def user_type(self):
        print('Customer User')

# Define a test funciton that will take an object called obj.

def test_function(obj):
    obj.is_admin()
    obj.user_type()

user_admin = Admin()
user_customer = Customer()

# Pass the created instance to test function
test_function(user_admin)
test_function(user_customer)

# Polymorphism with Class Methods
# Python uses two different class types in the same way

class TeamLead():
    def occupation(self):
        print('Team Lead')

    def hasAuth(self):
        print(True)

class TeamMember():
    def occupation(self):
        print('Team Member')

    def hasAuth(self):
        print(False)

tl1 = TeamLead()
tm1 = TeamMember()

# for person in (tl1, tm1):
#     person.occupation()


class Zuitt():
    def tracks(self):
        print("We are currently offering 3 tracks (developer career, pi-shape career, and short courses)")
    
    def num_of_hours(self):
        print('Learn web development in 360 hours!')

class DeveloperCareer(Zuitt):
    # Override the parent's num_of_hours() method
    def num_of_hours(self):
        print("Learn the basics of web development in 240 hours!")

class PiShapedCareer(Zuitt):
    # Override the parent's num_of_hours() method
    def num_of_hours(self):
        print("Learn skills for no-code app development in 140 hours!")

class ShortCourses(Zuitt):
    # Override the parent's num_of_hours() method
    def num_of_hours(self):
        print("Learn advanced topics in web development in 20 hours!")

# course1 = DeveloperCareer()
# course2 = PiShapedCareer()
# course3 = ShortCourses()

# course1.num_of_hours()
# course2.num_of_hours()
# course3.num_of_hours()

# -----------------------------------------------------------------

# [SECTION] Abstraction
# An abstract class that can be considered as a blueprint for other classes.

# Abstract Base Classes (abc)
# The import tells the program to get the abc module of python to be used
from abc import ABC, abstractclassmethod

class Polygon(ABC):
    @abstractclassmethod
    def print_number_of_sides(self):
        # This denotes that the method doesn't do anything.
        pass

class Triangle(Polygon):
    def __init__(self):
        super().__init__()

    # Since the Triangle class inherited the Polygon class it must now implement the abstace method.
    def print_number_of_sides(self):
        print("This polygon has 3 sides")

class Pentagon(Polygon):
    def __init__(self):
        super().__init__()

    def print_number_of_sides(self):
        print("This polygon has 5 sides")

shape1 = Triangle()
shape2 = Pentagon()

shape1.print_number_of_sides()
shape2.print_number_of_sides()